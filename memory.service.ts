export class MemoryService {
  public store: Object;

  constructor() {
    this.store = {};
  }

  public setItem<Type>(key: string, value: Type): void {
    this.store[key] = value;
  }

  public getItem<Type>(key: string): Type | null {
    return this.store[key] || null;
  }

  public removeItem(key: string): boolean {
    if (this.store[key]) {
      delete this.store[key];
      return true;
    }
    return false;
  }
}
