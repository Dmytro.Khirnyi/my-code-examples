import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {BehaviorSubject} from 'rxjs';
import {Observable} from "rxjs/Observable";


@Injectable()
export class UserService {

  /**
   * Root User stream
   */
  public user$: BehaviorSubject<IUser>;

  /**
   * Stream of auth state of user
   */
  public isAuth$: BehaviorSubject<boolean>;

  /**
   * Store user token
   * @type {string}
   * @private
   */
  public readonly _user_key: string = '__user';

  constructor(private storeService: StoreService,
              private router: Router) {

    this.initUser();
  }

  /**
   * Method to login user to system
   * @param user
   * @param isRemember
   */
  public signIn(user: IUser , isRemember: boolean): void {
    this.saveUser(user , isRemember);
  }

  /**
   * Method to log out user from service
   */
  public logOut(): void {
    Observable.fromPromise(this.router.navigate(['/']))
        .filter((state: boolean) => state)
        .subscribe(this.logOutHandler.bind(this));
  }

  private initUser(): void {
    /**
     * Getting user from store is exist
     * @type {IUser|null}
     */
    const user: IUser | null = this.getUser();
    this.user$ = new BehaviorSubject<IUser>(user);
    this.isAuth$ = new BehaviorSubject<boolean>(!!user);
  }

  private logOutHandler(): void {
    this.isAuth$.next(false);
    this.user$.next(null);
    this.clearUserInfo();
  }

  /**
   * Method to update User in store
   * @param user
   * @param isRemember
   */
  private saveUser(user: IUser, isRemember?: boolean): void {
    if (user) {
      this.isAuth$.next(true);
      this.user$.next(user);
      this.storeService.setItem(this._user_key, user, isRemember);
    }
  }

  /**
   * Method to remove user from our store
   */
  private clearUserInfo(): void {
    this.storeService.removeItem(this._user_key);
  }

  /**
   * Method to get user from storage
   * @returns {any|null}
   */
  private getUser(): IUser | null {
    return this.storeService.getItem(this._user_key) || null;
  }
}
