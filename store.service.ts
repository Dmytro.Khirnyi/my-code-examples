import {Injectable} from '@angular/core';

import {safe} from "./decorators.model";

@Injectable()
export class StoreService {

  @safe
  public getItem(key: string): any {
    return this.fromJson(sessionStorage.getItem(key)) || this.fromJson(localStorage.getItem(key));
  }

  @safe
  public setItem(key: string, valueObj: any, remember: boolean = true) {
    if (remember) {
      localStorage.setItem(key, this.toJson(valueObj));
    } else {
      sessionStorage.setItem(key, this.toJson(valueObj));
    }
  }

  @safe
  public removeItem(key: string) {
    sessionStorage.removeItem(key);
    localStorage .removeItem(key);
  }

  private fromJson(value: string): any {
    return JSON.parse(value);
  }

  private toJson(value: any): string {
    return JSON.stringify(value);
  }
}
