import 'zone.js/dist/zone-node';
import 'reflect-metadata';
import 'rxjs/Rx';
import * as express from 'express';
import * as cors from 'cors';

import { ServerAppModule } from './app/server-app.module';
import { ngExpressEngine } from './modules/ng-express-engine/express-engine';
import {ROUTES, serverRoutes} from './routes';
import { enableProdMode } from '@angular/core';
import {DEFAULT_API_URL, DEFAULT_PORT} from './main.config';
import {NextFunction, Request, Response} from "express";

enableProdMode();
const app = express();
const request = require('request');

app.use(cors());

const BACKEND_API_URL: string = 'http://' + (process.env.BACKEND_URL || DEFAULT_API_URL);
const port = process.env.PORT || DEFAULT_PORT;

app.engine('html', ngExpressEngine({
  bootstrap: ServerAppModule
}));

app.set('view engine', 'html');
app.set('views', 'src');

app.use('/', express.static('dist', {index: false}));

app.use((req: Request, res: Response, next: NextFunction) => {
  if (serverRoutes.some((route: string) => req.url.includes(route))) {
    req.pipe(parseRequest(req)).pipe(res);
  } else {
    next();
  }
});

ROUTES.forEach(route => {
  app.get(route, (req, res) => {
    res.render('../dist/index', {
      req: req,
      res: res,
      preboot: true
    });
  });
});

app.listen(port,() => {
	console.log(`Listening port: ${port}`);
});

function parseRequest(req: Request): Request {
  switch (req.method) {
    case 'POST':
      return request.post({
        uri: BACKEND_API_URL + req.url,
        json: req.body
      });
    case 'GET':
      return request(BACKEND_API_URL + req.url);
    case 'PUT':
      return request.put({
        uri: BACKEND_API_URL + req.url,
        json: req.body
      });
    case 'DELETE':
      return request.delete({
        uri: BACKEND_API_URL + req.url,
        json: req.body
      });
  }
}
