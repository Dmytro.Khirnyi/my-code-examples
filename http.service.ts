import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Headers, URLSearchParams, Http, Request, Response, ResponseContentType} from '@angular/http';

/**
 * Custom http service
 */
@Injectable()
export class HttpService {

    /**
     * Default Http request Method
     * @type {string}
     */
    public readonly defaultMethod: string = 'GET';

    /**
     * Default Http request Error object
     * @type {{status: string; message: string}}
     */
    public readonly defaultError: IErrorResponse = {
        status: 'Fail',
        message: 'Server error.'
    };

    constructor(private http: Http,
                private userService: UserService) {}

    /**
     * Method to send Request
     * @param options
     * @returns {Observable<any>}
     */
    public request<Type>(options: IHttpRequest): Observable<Type> {
        const body: any = HttpService.getRequestBody(options);

        return this.processRequest(this.createRequest(options, body), options);
    }

    private static getRequestBody(options: IHttpRequest): any {
        switch (options.encoding) {
            case 'url':
                return HttpService.urlEncodeBody(options.body);
            case 'fd':
                return HttpService.formDataBody(options.body);
            default:
                return options.body;
        }
    }

    /**
     * Method to create http object
     * @param options
     * @param body
     * @returns {any}
     */
    private createRequest(options: IHttpRequest, body: any): Request {
        return new Request({
            method: options.method || this.defaultMethod,
            url: HttpService.getUrl(options),
            body,
            headers: this.getHeaders(options),
            params: options.searchParams ? HttpService.urlEncodeBody(options.searchParams) : null,
            responseType: options.isBlob ? ResponseContentType.Blob : ResponseContentType.Json
        })
    }

    /**
     * Method to send http request
     * @param req
     * @param options
     * @returns {Promise<R>}
     */
    private processRequest<Type>(req: Request, options: IHttpRequest): Observable<Type> {
        return this.http
            .request(req)
            .map(res => options.isBlob ? res.blob() : res.json())
            .catch(this.errorHandler.bind(this));
    }

    /**
     * Method to handle error
     * @param err
     * @returns {any}
     */
    private errorHandler(err: Response | undefined): Observable<IErrorResponse>{
        let errObject: IErrorResponse | null = null;
        if ((err instanceof Response) && HttpService.checkResType(err, 'json')) {
            if (errObject && errObject.error) {
                errObject = errObject.error;
            }
        }
        return Observable.throw(errObject || this.defaultError);
    }

    /**
     * Method to get Headers object
     * @param options
     * @returns {Headers}
     */
    private getHeaders(options: IHttpRequest): Headers {
        const headers: Headers = new Headers();

        if (options.userToken) {
            const user: IUser = this.userService.user$.getValue();
            if (user && user.token) {
                headers.append('authorization', user.token);
            }
        }

        return headers;
    }

    /**
     * Method to get url encoded object
     * @param data
     * @returns {URLSearchParams}
     */
    public static urlEncodeBody(data: Object): URLSearchParams {
        const params: URLSearchParams = new URLSearchParams();
        Object.keys(data)
            .filter((key: string) => data.hasOwnProperty(key))
            .forEach((key) => params.set(key, data[key]));
        return params;
    }

    /**
     * Method to parse Object to FormData
     * @param data
     * @returns {FormData}
     */
    public static formDataBody(data: Object): FormData {
        const fd: FormData = new FormData();
        Object.keys(data)
            .filter((key: string) => data.hasOwnProperty(key))
            .forEach((key) => fd.append(key, data[key]));
        return fd;
    }

    /**
     * Method to check response content type
     * @param res
     * @param type
     * @returns {string|boolean}
     */
    public static checkResType(res: Response, type: string = 'json') {
        const resType = res.headers.get('Content-Type');
        return resType && (resType.indexOf(type) !== -1);
    }

    /**
     * Method to get url of request
     * @param options
     * @returns {string}
     */
    private static getUrl(options: IHttpRequest): string {
        return options.absolutePath ? options.url : BACKEND_SERVER_URL + options.url;
    }
}