/**
 * Custom Http Request model
 */
export interface HttpRequest {
  url: string;
  method?: string;
  encoding?: string;
  searchParams?: Object;
  body?: any;
  setToHeaders?: Object;
  userToken?: boolean;
  absolutePath?: boolean;
  isBlob?: boolean;
}

/**
 * Model that describe response if result is Error
 */
export interface ErrorResponse {
  status: string;
  message: string;
  error?: any;
}

export interface User {
  readonly id?: number;
  readonly token: string;
}