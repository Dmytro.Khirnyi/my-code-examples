export function safe(target: Object, propertyKey: string, descriptor: TypedPropertyDescriptor<any>): TypedPropertyDescriptor<any> {
    const originalMethod = descriptor.value;
    // Подменяем ее на нашу обертку
    descriptor.value = function SafeWrapper () {
        try {
            originalMethod.apply(this, arguments);
        } catch(ex) {
            console.error(ex);
        }
    };
    return descriptor;
}